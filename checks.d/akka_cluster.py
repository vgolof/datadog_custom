# stdlib
from datetime import datetime
import os.path
import socket
import ssl
import time
import re
import json
import requests
from urlparse import urlparse
#from pprint import pprint

# 3rd party
import tornado
import requests

# project
from checks.network_checks import Status, EventType
from checks import AgentCheck
from config import _is_affirmative
from util import headers as agent_headers

#main check
class AkkaClusterHTTPCheck(AgentCheck):
    SOURCE_TYPE_NAME = 'system'
    SC_STATUS = 'http.can_connect'
    SC_SSL_CERT = 'http.ssl_cert'
    
# Not work :-\ NotImplementedError
#    def __init__(self, name, init_config, agentConfig, instances=None):
#        self.ca_certs = init_config.get('ca_certs', get_ca_certs_path())
#        AgentCheck.__init__(self, name, init_config, agentConfig, instances)


    # Parse instance
    def _load_conf(self, instance):
        self.log.debug("_load_conf")
        name = instance['name']
        url = instance.get('url')
#reserved
#        insts =  int(instance.get('insts_total', 0))
#        inst_name
#        inst_addr
        timeout = int(instance.get('timeout', 10))
    
        if not url:
            raise Exception("Bad configuration. You must specify a url")

        self.log.debug("url %s , name %s"
                      % (url, instance['name']))
        return name, url, timeout

    def check(self, instance):
        name, addr, timeout = self._load_conf(instance)
        start = time.time()

        service_checks = []
        http_response_status_code = str("(1|2|3)\d\d")
        tags = []
        response_time = True
        headers = agent_headers(self.agentConfig)
        disable_ssl_validation = True

        try:
            self.log.debug("Connecting to %s" % addr)
            if urlparse(addr)[0] == "https":
                self.warning("Skipping SSL certificate validation for %s based on configuration"
                             % addr)
            #get json
            status = "400"
            r = requests.get(addr, auth=None, timeout=timeout, headers=headers,
                             verify=not disable_ssl_validation)
            status = str(r.status_code)

        except socket.timeout, e:
            length = int((time.time() - start) * 1000)
            self.log.info("%s is DOWN, error: %s. Connection failed after %s ms"
                          % (addr, str(e), length))
            service_checks.append((
                self.SC_STATUS,
                Status.DOWN,
                "%s. Connection failed after %s ms" % (str(e), length)
            ))

        except requests.exceptions.ConnectionError, e:
            length = int((time.time() - start) * 1000)
            self.log.info("%s is DOWN, error: %s. Connection failed after %s ms"
                          % (addr, str(e), length))
            service_checks.append((
                self.SC_STATUS,
                Status.DOWN,
                "%s. Connection failed after %s ms" % (str(e), length)
            ))

        except socket.error, e:
            length = int((time.time() - start) * 1000)
            self.log.info("%s is DOWN, error: %s. Connection failed after %s ms"
                          % (addr, repr(e), length))
            service_checks.append((
                self.SC_STATUS,
                Status.DOWN,
                "Socket error: %s. Connection failed after %s ms" % (repr(e), length)
            ))

        except Exception, e:
            length = int((time.time() - start) * 1000)
            self.log.error("Unhandled exception %s. Connection failed after %s ms"
                           % (str(e), length))
#            raise

        # Only report this metric if the site is not down
        if response_time and not service_checks:
            # Stop the timer as early as possible
            running_time = time.time() - start
            # Store tags in a temporary list so that we don't modify the global tags data structure
            tags_list = list(tags)
            tags_list.append('url:%s' % addr)
            self.gauge('akkaclcheck.http.response_time', running_time, tags=tags_list)

        # Check HTTP response status code
        if not (service_checks or re.match(http_response_status_code, status)):
            self.log.info("Incorrect HTTP return code. Expected %s, got %s"
                          % (http_response_status_code, status))
            service_checks.append((
                self.SC_STATUS,
                Status.DOWN,
                "Incorrect HTTP return code. Expected %s, got %s"
                % (http_response_status_code, status)
            ))
#        self.log.info("Readed JSON %s"
#                      % (r.content))
        self.log.debug("Status = %s"
                      % (status))
        numinsts = 0
        if (re.match(http_response_status_code, status)):
            jdata = json.loads(r.content)
#            pprint(jdata)
            for s in jdata:
                numinsts = numinsts+1
            self.log.debug("jdata instss %d"
                          % (numinsts))
        
        self.akka_cluster_service_check(name, numinsts, status, instance, msg=None)

    #return result
    def akka_cluster_service_check(self, sc_name, numinsts, status, instance, msg=None):
        instance_name = instance['name']
        url = instance.get('url', None)
        tags = ['url:{0}'.format(url), "instance:{0}".format(instance_name)]

        self.gauge('akkaclcheck.insts.count', numinsts, tags=tags)

        self.service_check(sc_name,
                           status,
                           tags=tags,
                           message=msg
                           )

#multiinstance
if __name__ == '__main__':
    check, instances = AkkaClusterHTTPCheck.from_yaml('/etc/dd-agent/conf.d/akka_clustert.yaml')
    for instance in instances:
#        print "\nRunning the check against url: %s" % (instance['url'])
        check.check(instance)
        if check.has_events():
            print 'Events: %s' % (check.get_events())
        print 'Metrics: %s' % (check.get_metrics())
